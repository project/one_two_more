<?php

/**
 * @file
 * Provides a Node reference field formatter.
 */

/**
 * Implements hook_field_formatter_info().
 */
function one_two_more_field_formatter_info() {
  $formatters =  array(
    // This formatter displays your youtube video.
    'one_two_more' => array(
      'label' => t('One, Two, more'),
      'field types' => array('entityreference'),
      'settings' => array(
        'link' => FALSE,
      ),
    ),
  );

  return $formatters;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function one_two_more_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $element['link'] = array(
    '#title' => t('Link label to the referenced entity'),
    '#type' => 'checkbox',
    '#default_value' => $settings['link'],
  );

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function one_two_more_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  if ($settings['link']) {
    $summary = t('One, Two, more - With link');
  }
  else {
    $summary = t('One, Two, more - No link');
  }

  return $summary;
}

/**
 * Implements hook_field_formatter_view().
 */
function one_two_more_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $settings = $display['settings'];

  $output_items = array();
    foreach ($items as $delta => $item) {
    if ($delta < 2) {
      $referenced_entity = reset(entity_load($field['settings']['target_type'], array($item['target_id'])));
      switch ($field['settings']['target_type']) {
        case 'node':
          $label = $referenced_entity->title;
          break;
        case 'user':
          $label = $referenced_entity->name;
          break;
      }
      // If the link is to be displayed and the entity has a uri, display a link.
      // Note the assignment ($url = ) here is intended to be an assignment.
      if ($display['settings']['link'] && ($uri = entity_uri($field['settings']['target_type'], $referenced_entity))) {
        $output_items[] = l($label, $uri['path'], $uri['options']);
      }
      else {
        $output_items[] = check_plain($label);
      }
    }
  }

  // One.
  if (count($items) === 1) {
    $element = array(array('#markup' => reset($output_items)));
  }

  // Two.
  else if (count($items) == 2) {
    $element = array(array('#markup' => implode(' & ', $output_items)));
  }

  // More.
  else if (count($items) > 2) {
    $element = array(array('#markup' => implode(', ', $output_items) . t(' & more')));
  }

  return $element;
}